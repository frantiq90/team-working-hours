<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

class AbstractController extends Controller
{
    const TEAM_WORKING_HOURS_CACHING_ENABLED = 'TEAM_WORKING_HOURS_CACHING_ENABLED';
    const TEAM_WORKING_HOURS_CACHING_TIME = 'TEAM_WORKING_HOURS_CACHING_TIME';

    const JSON_RESPONSE_OK = 200;
    const JSON_RESPONSE_SAVED = 201;
    const JSON_RESPONSE_NOT_SAVED = 204;
    const JSON_RESPONSE_NOT_FOUND = 404;

    /**
     * Is caching enabled.
     *
     * @var bool
     */
    protected $isCachingEnabled;
    /**
     * Caching time(minutes).
     *
     * @var int
     */
    protected $cachingTime;

    /**
     * AbstractController constructor.
     */
    public function __construct()
    {
        $this->isCachingEnabled = $this->isCachingEnabled();
        $this->cachingTime = $this->getCachingTime();
    }

    /**
     * Check is chaching enabled.
     *
     * @return bool
     */
    protected function isCachingEnabled()
    {
        $isCachingEnabled = $_ENV[self::TEAM_WORKING_HOURS_CACHING_ENABLED];

        if ($isCachingEnabled === 'true') {
            $isCachingEnabled = true;
        } else {
            $isCachingEnabled = false;
        }

        return $isCachingEnabled;
    }

    /**
     * Get caching time(in minutes).
     * @return int
     */
    protected function getCachingTime()
    {
        return (int)$_ENV[self::TEAM_WORKING_HOURS_CACHING_TIME];
    }
}
