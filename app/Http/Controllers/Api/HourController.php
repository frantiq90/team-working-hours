<?php

namespace App\Http\Controllers\Api;

use App\Models\HourInterface;
use App\Models\Worker;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class HourController extends AbstractController
{
    const WORKER_HOURS_ARRAY = 'worker_hours_array';

    /**
     * Show resource.
     *
     * @param $workerId
     *
     * @return JsonResponse
     */
    public function show($workerId)
    {
        try {
            $cachedWorkerHours = Cache::get(self::WORKER_HOURS_ARRAY);

            if ($cachedWorkerHours !== null && $this->isCachingEnabled === true) {
                $workerHours = $cachedWorkerHours;
            } else {
                $workerHours = Worker::findOrFail($workerId)->hours()->get();
                Cache::put(self::WORKER_HOURS_ARRAY, $workerHours, $this->cachingTime);
            }

            return response()->json($workerHours, self::JSON_RESPONSE_OK);
        } catch (ModelNotFoundException $exception) {
            return response()->json('Worker not found', self::JSON_RESPONSE_NOT_FOUND);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @param HourInterface $hour
     *
     * @return JsonResponse
     */
    public function store(Request $request, HourInterface $hour)
    {
        try {
            $validatedData = $request->validate([
                'worker_id' => 'required',
                'date' => 'required',
                'from' => 'required',
                'to' => 'required'
            ]);

            $hour->fill($validatedData);
            $hour->save();
            Cache::put(self::WORKER_HOURS_ARRAY, null, $this->cachingTime);

            return response()->json('Worker hours added', self::JSON_RESPONSE_SAVED);
        } catch (\Exception $exception) {
            return response()->json('Error. Worker hours not added', self::JSON_RESPONSE_NOT_SAVED);
        }
    }
}
