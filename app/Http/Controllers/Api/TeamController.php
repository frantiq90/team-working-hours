<?php

namespace App\Http\Controllers\Api;

use App\Models\DateFormatInterface;
use App\Models\Team;
use App\Models\TeamInterface;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Validator;

class TeamController extends AbstractController implements DateFormatInterface
{
    const TEAM_WORKING_HOURS_ARRAY = 'team_working_hours_array';
    /**
     * Team model.
     *
     * @var Team
     */
    private $team;

    /**
     * TeamController constructor.
     *
     * @param TeamInterface $team
     */
    public function __construct(TeamInterface $team)
    {
        parent::__construct();

        $this->team = $team;
    }

    /**
     * Index action.
     *
     * @param $days
     * @param $fromDate
     *
     * @return JsonResponse
     */
    public function index($days, $fromDate)
    {
        try {
            $validateFailure = Validator::make(
                [
                    'days' => $days,
                    'from_date' => $fromDate
                ],
                [
                    'days' => 'required|integer',
                    'from_date' => 'required|date|date_format:' . self::DATE_FORMAT
                ]
            )->fails();

            if ($validateFailure === true) {
                throw new Exception('Wrong params');
            }

            $cachedTeamHours = Cache::get(self::TEAM_WORKING_HOURS_ARRAY);

            if ($cachedTeamHours !== null && $this->isCachingEnabled === true) {
                $teamHours = $cachedTeamHours;
            } else {
                $teamHours = $this->team->getTeamWorkingHours($days, $fromDate);
                Cache::put(
                    'days_' . $days . 'from_' . $fromDate . self::TEAM_WORKING_HOURS_ARRAY,
                    $teamHours,
                    $this->cachingTime
                );
            }

            return response()->json($teamHours, self::JSON_RESPONSE_OK);
        } catch (Exception $exception) {
            return response()->json($exception->getMessage(), self::JSON_RESPONSE_NOT_FOUND);
        }
    }
}
