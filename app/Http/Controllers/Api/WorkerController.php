<?php

namespace App\Http\Controllers\Api;

use App\Models\Worker;
use App\Models\WorkerInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class WorkerController extends AbstractController
{
    const TEAM_WORKERS_ARRAY = 'team_workers_array';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $cachedWorkers = Cache::get(self::TEAM_WORKERS_ARRAY);

            if ($cachedWorkers !== null && $this->isCachingEnabled === true) {
                $workers = $cachedWorkers;
            } else {
                $workers = $workers = Worker::all();
                Cache::put(self::TEAM_WORKERS_ARRAY, $workers, $this->cachingTime);
            }

            return response()->json($workers, self::JSON_RESPONSE_OK);
        } catch (\Exception $exception) {
            return response()->json('No data', self::JSON_RESPONSE_NOT_FOUND);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param WorkerInterface $worker
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, WorkerInterface $worker)
    {
        try {
            $validatedData = $request->validate([
                'name' => 'required'
            ]);

            $worker->fill($validatedData);
            $worker->save();
            Cache::put(self::TEAM_WORKERS_ARRAY, null, $this->cachingTime);

            return response()->json('Worker saved', self::JSON_RESPONSE_SAVED);
        } catch (\Exception $exception) {
            return response()->json('Can not save new worker', self::JSON_RESPONSE_NOT_SAVED);
        }
    }
}
