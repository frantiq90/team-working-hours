<?php

namespace App\Models;

interface DateFormatInterface
{
    CONST DATE_FORMAT = 'Y-m-d';
    CONST TIME_FORMAT = 'H:i:s';
    CONST TIME_FORMAT_FOR_OUTPUT = 'H:i';
}