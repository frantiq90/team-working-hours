<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Hour extends Model implements HourInterface
{
    /**
     * Fillable array.
     *
     * @var array
     */
    protected $fillable = [
        'worker_id',
        'date',
        'from',
        'to'
    ];

    /**
     * Get hour related worker.
     *
     * @return BelongsTo|mixed
     */
    public function worker()
    {
        return $this->belongsTo(Worker::class);
    }

    /**
     * Get working hours by given date.
     *
     * @param $date
     *
     * @return mixed
     */
    public function getWorkingHoursByDate($date)
    {
        $workingHours = self::where('date', '=', $date)->get()->toArray();

        return $workingHours;
    }
}
