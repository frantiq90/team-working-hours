<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\BelongsTo;

interface HourInterface
{
    /**
     * Get hour related worker.
     *
     * @return BelongsTo|mixed
     */
    public function worker();

    /**
     * Get working hours by given date.
     *
     * @param $date
     *
     * @return mixed
     */
    public function getWorkingHoursByDate($date);
}
