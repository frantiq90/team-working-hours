<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;

class Team implements TeamInterface, DateFormatInterface
{
    const START_TIMESTAMP = 'start_timestamp';
    const FINISH_TIMESTAMP = 'finish_timestamp';
    /**
     * Hour object.
     *
     * @var Hour
     */
    private $hour;

    /**
     * Slots array.
     *
     * @var array
     */
    private $slots;

    /**
     * Team constructor.
     *
     * @param Hour  $hour
     * @param array $slots
     */
    public function __construct(Hour $hour, array $slots = [])
    {
        $this->hour = $hour;
        $this->slots = $slots;
    }

    /**
     * Get team working hours.
     *
     * @param $days
     * @param $fromDate
     *
     * @return array
     */
    public function getTeamWorkingHours($days, $fromDate)
    {
        $datesArray = $this->getDates($days, $fromDate);
        $workingHoursArray = $this->mapDatesToWorkingHours($datesArray);
        $teamSlotsArray = $this->getTeamSlots($workingHoursArray);

        return $teamSlotsArray;
    }

    /**
     * Get all workers.
     *
     * @return Collection|static[]
     */
    public function workers()
    {
        $workers = Worker::all();

        return $workers;
    }

    /**
     * Get requested dates.
     *
     * @param $days
     * @param $fromDate
     *
     * @return array
     */
    private function getDates($days, $fromDate)
    {
        $dates = [];

        for ($days--; $days >= 0; $days--) {
            $dateToAdd = Carbon::createFromFormat(self::DATE_FORMAT, $fromDate)
                ->addDays($days)
                ->format(self::DATE_FORMAT);
            $dates[] = $dateToAdd;
        }

        return $dates;
    }

    /**
     * Map dates to working hours array.
     *
     * @param $datesArray
     *
     * @return array
     */
    private function mapDatesToWorkingHours($datesArray)
    {
        $workingHoursArray = [];

        foreach ($datesArray as $date) {
            $workingHoursArray[] = [
                'date' => $date,
                'team_working_hours' =>  $this->hour->getWorkingHoursByDate($date)
            ];
        }

        return $workingHoursArray;
    }

    /**
     * Get team slots.
     *
     * @param $workingHoursArray
     *
     * @return array
     */
    private function getTeamSlots($workingHoursArray)
    {
        $teamSlots = [];

        foreach ($workingHoursArray as $singleDayHours) {
            $isEmpty = empty($singleDayHours['team_working_hours']);
            if ($isEmpty === true) {
                $teamSlots[] = [
                    'date' => $singleDayHours['date'],
                    'slots' => []
                ];
            } else {
                $teamSlots[] = [
                    'date' => $singleDayHours['date'],
                    'slots' => $this->getAvailableSlots($singleDayHours)
                ];
            }
        }

        return $this->convertToReadableHours($teamSlots);
    }

    /**
     * Get available slots.
     *
     * @param $singleDayHours
     *
     * @return array
     */
    private function getAvailableSlots($singleDayHours)
    {
        $workingHours = $singleDayHours['team_working_hours'];

        // Reset array for new day.
        $this->slots = [];

        foreach ($workingHours as $hour)
        {
            $startDateTime = $hour['date'] . ' ' . $hour['from'];
            $finishDateTime = $hour['date'] . ' ' . $hour['to'];

            $startTimestamp = Carbon::createFromFormat(
                self::DATE_FORMAT . ' ' . self::TIME_FORMAT,
                $startDateTime
            )->timestamp;
            $finishTimestamp = Carbon::createFromFormat(
                self::DATE_FORMAT . ' ' . self::TIME_FORMAT,
                $finishDateTime
            )->timestamp;

            $areSlotsEmpty = empty($this->slots);

            if ($areSlotsEmpty === false) {
                // Check is current slot exists if no then add it
                $isCurrentSlotExists = $this->checkIsSlotAlreadyExist($startTimestamp, $finishTimestamp);

                // If current slot is completely does not exists then we can add.
                if ($isCurrentSlotExists === false) {
                    // This logic will try to extend any exiting slot first, before adding.
                    $this->addSlot($startTimestamp, $finishTimestamp);
                }
            } else {
                $this->slots[] = [
                    $startTimestamp,
                    $finishTimestamp
                ];
            }
        }

        return $this->slots;
    }

    /**
     * Convert all slots timestamps to human readable hours.
     *
     * @param $teamSlots
     * @return mixed
     */
    private function convertToReadableHours($teamSlots)
    {
        $numberOfDays = count($teamSlots);

        for ($i = 0; $i < $numberOfDays; $i++) {
            if (empty($teamSlots[$i]['slots']) === false) {
                $numberOfSlots = count($teamSlots[$i]['slots']);

                for ($j = 0; $j < $numberOfSlots; $j++) {
                    $teamSlots[$i]['slots'][$j][0] = Carbon::createFromTimestamp(
                            $teamSlots[$i]['slots'][$j][0]
                        )
                        ->format(self::TIME_FORMAT_FOR_OUTPUT);

                    $teamSlots[$i]['slots'][$j][1] = Carbon::createFromTimestamp(
                            $teamSlots[$i]['slots'][$j][1]
                        )
                        ->format(self::TIME_FORMAT_FOR_OUTPUT);
                }
            }
        }

        return $teamSlots;
    }

    /**
     * Check is slot alredy exists.
     *
     * @param $startTimestamp
     * @param $finishTimestamp
     *
     * @return bool
     */
    private function checkIsSlotAlreadyExist($startTimestamp, $finishTimestamp)
    {
        $isSlotExists = false;

        foreach ($this->slots as $slot) {
            if ($slot[0] === $startTimestamp && $slot[1] === $finishTimestamp) {
                $isSlotExists = true;
            }
        }

        return $isSlotExists;
    }

    /**
     * Add slot.
     *
     * @param $startTimestamp
     * @param $finishTimestamp
     */
    private function addSlot($startTimestamp, $finishTimestamp)
    {
        $tryExtendAnyExistingSlot = $this->tryExtendAnyExistingSlot($startTimestamp, $finishTimestamp);

        if ($tryExtendAnyExistingSlot === false) {
            $this->slots[] = [
                $startTimestamp, $finishTimestamp
            ];
        }
    }

    /**
     * Try to extend any existing slot.
     *
     * @param $startTimestamp
     * @param $finishTimestamp
     *
     * @return bool
     */
    private function tryExtendAnyExistingSlot($startTimestamp, $finishTimestamp)
    {
        $isAnySlotExtended = false;

        foreach ($this->slots as $slotNumber => $slotValues) {
            // Timeline inner extend.
            if (
            ($startTimestamp >= $slotValues[0] && $startTimestamp <= $slotValues[1])
            && ($finishTimestamp >= $slotValues[0] && $finishTimestamp <= $slotValues[1])
            ) {
                $isAnySlotExtended = true;
            }

            // Timeline outer extend.
            if (
                $startTimestamp <= $slotValues[0]
                && $finishTimestamp >= $slotValues[1]
                && ($isAnySlotExtended === false)
            ) {
                // Extend start and finish time of slot.
                $startTimeInteract = $this
                    ->checkInterActionWithAnySlot($startTimestamp, self::START_TIMESTAMP);
                $finishTimeInteract = $this
                    ->checkInterActionWithAnySlot($finishTimestamp, self::FINISH_TIMESTAMP);

                if ($startTimeInteract === false) {
                    $this->slots[$slotNumber][0] = $startTimestamp;
                } else {
                    $this->slots[$slotNumber][0] = $startTimeInteract;
                }

                if ($finishTimeInteract === false) {
                    $this->slots[$slotNumber][1] = $finishTimestamp;
                } else {
                    $this->slots[$slotNumber][1] = $finishTimeInteract;
                }

                $isAnySlotExtended = true;
            }

            // Timeline left extend.
            if ($startTimestamp < $slotValues[0]
                && ($finishTimestamp >= $slotValues[0] && $finishTimestamp <= $slotValues[1])

                && ($isAnySlotExtended === false)
            ) {
                // Extend start time of slot.
                $startTimeInteract = $this
                    ->checkInterActionWithAnySlot($startTimestamp, self::START_TIMESTAMP);

                if ($startTimeInteract === false) {
                    $this->slots[$slotNumber][0] = $startTimestamp;
                } else {
                    $this->slots[$slotNumber][0] = $startTimeInteract;
                }

                $isAnySlotExtended = true;
            }

            // Timeline right extend.
            if ($finishTimestamp > $slotValues[1]
                && ($startTimestamp <= $slotValues[1] && $startTimestamp >= $slotValues[0])

                && ($isAnySlotExtended === false)
            ) {
                // Extend start time of slot.
                $finishTimeInteract = $this
                    ->checkInterActionWithAnySlot($finishTimestamp, self::FINISH_TIMESTAMP);

                if ($finishTimeInteract === false) {
                    $this->slots[$slotNumber][1] = $finishTimestamp;
                } else {
                    $this->slots[$slotNumber][1] = $finishTimeInteract;
                }

                $isAnySlotExtended = true;
            }
        }

        return $isAnySlotExtended;
    }

    /**
     * Check is slot interact with any other slot.
     *
     * @param $givenTimestamp
     * @param $switch
     *
     * @return bool|integer
     */
    private function checkInterActionWithAnySlot($givenTimestamp, $switch)
    {
        // If interact get start/finish timestamp and unset this slot.
        switch ($switch) {
            case self::START_TIMESTAMP:
                foreach ($this->slots as $slotKey => $slotValue) {
                    if ($givenTimestamp >= $slotValue[0] && $givenTimestamp <= $slotValue[1]) {
                        $timestamp = $slotValue[0];

                        return $timestamp;
                    } else {
                        return false;
                    }
                }
                break;
            case self::FINISH_TIMESTAMP:
                foreach ($this->slots as $slotKey => $slotValue) {
                    if ($givenTimestamp >= $slotValue[0] && $givenTimestamp <= $slotValue[1]) {
                        $timestamp = $slotValue[1];

                        return $timestamp;
                    } else {
                        return false;
                    }
                }
                break;
            default:
                return false;
        }
    }
}
