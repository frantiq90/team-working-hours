<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;

interface TeamInterface
{
    /**
     * Get team working hours.
     *
     * @param $days
     * @param $fromDate
     *
     * @return array
     */
    public function getTeamWorkingHours($days, $fromDate);

    /**
     * Get all workers.
     *
     * @return Collection|static[]
     */
    public function workers();
}
