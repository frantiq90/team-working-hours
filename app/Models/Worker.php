<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Worker extends Model implements WorkerInterface
{
    /**
     * Fillable array.
     *
     * @var array
     */
    protected $fillable = ['name'];

    /**
     * Get worker working hours.
     *
     * @return HasMany
     */
    public function hours()
    {
        return $this->hasMany(Hour::class);
    }
}
