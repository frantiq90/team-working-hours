<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\HasMany;

interface WorkerInterface
{
    /**
     * Get worker working hours.
     *
     * @return HasMany
     */
    public function hours();
}
