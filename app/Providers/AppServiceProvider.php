<?php

namespace App\Providers;

use App\Models\Hour;
use App\Models\HourInterface;
use App\Models\Team;
use App\Models\TeamInterface;
use App\Models\Worker;
use App\Models\WorkerInterface;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(TeamInterface::class, Team::class);
        $this->app->bind(HourInterface::class, Hour::class);
        $this->app->bind(WorkerInterface::class, Worker::class);
    }
}
