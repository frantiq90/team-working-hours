<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class HoursTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('hours')->insert([
            'worker_id' => 1,
            'date' => '2016-10-02',
            'from' => '14:00:00',
            'to' => '20:00:00',
            'updated_at' => Carbon::now()->toDateTimeString(),
            'created_at' => Carbon::now()->toDateTimeString()
        ]);

        DB::table('hours')->insert([
            'worker_id' => 1,
            'date' => '2016-10-04',
            'from' => '9:00:00',
            'to' => '11:00:00',
            'updated_at' => Carbon::now()->toDateTimeString(),
            'created_at' => Carbon::now()->toDateTimeString()
        ]);

        DB::table('hours')->insert([
            'worker_id' => 2,
            'date' => '2016-10-04',
            'from' => '10:00:00',
            'to' => '13:00:00',
            'updated_at' => Carbon::now()->toDateTimeString(),
            'created_at' => Carbon::now()->toDateTimeString()
        ]);

        DB::table('hours')->insert([
            'worker_id' => 3,
            'date' => '2016-10-04',
            'from' => '6:00:00',
            'to' => '7:00:00',
            'updated_at' => Carbon::now()->toDateTimeString(),
            'created_at' => Carbon::now()->toDateTimeString()
        ]);

        DB::table('hours')->insert([
            'worker_id' => 3,
            'date' => '2016-10-05',
            'from' => '6:00:00',
            'to' => '7:00:00',
            'updated_at' => Carbon::now()->toDateTimeString(),
            'created_at' => Carbon::now()->toDateTimeString()
        ]);
    }
}
