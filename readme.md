# DESCRIPTION

Laravel micro-service providing API to get and store team working hours.

# INSTALLATION 

Please setup your enviroment according to Laravel Documentation, then map an IP to virtual host name.
Example: go to /etc/hosts and add ``192.168.1.10 teamworkinghours.test``.
After than please run the following artisan commands in project root:

``composer update``

``composer install``

``composer dump-autoload``

``php artisan migrate``

``php artisan db:seed``

Make sure the redis daemon is installed in the system environment(optionally chech the predis/predis is installed in vendor folder).
Check the .env keys:

``TEAM_WORKING_HOURS_CACHING_ENABLED`` - should be set to true to enable caching. Otherwise switch it to false.

``TEAM_WORKING_HOURS_CACHING_TIME`` - this key is keeping the values of how many minutes cache will be kept.
 
# API ENDPOINTS


``GET http://teamworkinghours.test/api/team/workers`` - list of all workers,

``POST http://teamworkinghours.test/api/team/workers`` - save worker only `name` field is required,

``GET http://teamworkinghours.test/api/workers/hours/{worker_id}`` - list of given worker id hours,

``POST http://teamworkinghours.test/api/workers/hours`` - save given worker hours, required `worked_id`, `from`, `date`, format: woker_id: integer, from: date:H:m, to: date:H:m, date: date:Y-m-d

``GET http://teamworkinghours.test/api/team/hours/days/{days}/from_date/{from_date}`` - format days: integer, from_date: date: Y-m-d,
Example: 'http://teamworkinghours.test/api//team/hours/days/5/from_date/2018-07-01'


# TESTS

To run basic api test please run ``phpunit`` in project root directory.

Thank you all for testing, using and reviewing this code.