<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::resource('/team/workers', 'Api\WorkerController')->only([
    'index', 'store'
]);
Route::resource('/workers/hours', 'Api\HourController')->only([
    'store', 'show'
]);
Route::get('/team/hours/days/{days}/from_date/{from_date}', 'Api\TeamController@index');
