<?php

namespace Tests\Unit;

use App\Models\Worker;
use Tests\TestCase;

class TeamHoursApiTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testCanGetTeamWorkingHours()
    {
        $response = $this->get(asset('/api/team/hours/days/5/from_date/2018-07-01'));

        $response->assertStatus(200);
    }

    public function testCanAddNewWorkerTimeSlot()
    {
        $worker = new Worker();
        $worker
            ->fill(['name' => 'test' . random_int(0,100000)])
            ->save();

        $data = [
            'worker_id' => $worker->id,
            'from' => '14:00',
            'to' => '15:30',
            'date' => '2018-07-08'
        ];

        $response = $this->post(asset('/api/workers/hours/'), $data);

        $response->assertStatus(201);

        try {
            $worker->delete();
        } catch (\Exception $exception) {
            echo $exception->getMessage();
        }
    }
}
